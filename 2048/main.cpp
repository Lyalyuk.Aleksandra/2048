#include<iostream>
#include<glut.h>
//#include<gl/GLAux.h>
#include<gl/GL.h>
#include "Canvas.h"

using namespace std;

Canvas* canvas = new Canvas();

bool st = true;

void Init(void)
{
	glClearColor(1.0,1.0,1.0,1.0); //���� ������� ������
	glColor3f(0.5f,0.5f,0.5f); //�������� ����
	glPointSize(1.1); //������ �����
	glMatrixMode(GL_PROJECTION); //������� �������������� �������� ��� ����� ����� ���� ���������� ��� ��� �������� ��� ����� ��� ������������� ������� � ����� ���������
	glLoadIdentity(); //��������� ��� ���
	gluOrtho2D(0,256,0,256); //����� ����� ��� ��������� ���� ��� �� 0 �� 256 ����� �� 0 �� 256
	canvas->load(); //��������� ��������
};

void pole()
{
	if (st)
	{
		glClear(GL_COLOR_BUFFER_BIT); //�������
		glBegin(GL_LINES); //����� ������ �����
			for(int i = 1; i < 5;i++)
			{
				glVertex2i(64*i,0);
				glVertex2i(64*i,256);
				glVertex2i(0,64*i);
				glVertex2i(256,64*i);
			};
		glEnd();
		canvas->draw(); //������ ������ �����
		glFlush(); //������� �� �����
	}
};

void mouseFunc(int but,int state,int x,int y) //������� ������������ ��� ������� ������ ������� ����� ������ �� ����� ��������� ��� ������, ������� ������ ������ ��������� ��� ������ �� ������ ���� ����� �������� ���� ��������� �������
{
	if (st)
	{
		y  = 512 - y;
		if (but == GLUT_LEFT_BUTTON)
		{
			if(state == GLUT_DOWN)
			{
				canvas->move(x,y);
				canvas->setPrevState(x,y);

			} else
			{
				canvas->move(x,y);
				canvas->stopMoving();
			};
		} else
		{
			canvas->stopMoving();
		};
		pole();
		glFlush();
		if (canvas->endChecker())
		{
			st = false;
			//delete canvas;
		}
	}
};

void action(unsigned char key,int x,int y)
{
	if ((key == 13) && (!st)) //��� ������ �� ����� ���
	{
		st = true;
		canvas = new Canvas();
		canvas->load();
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc,argv); //�������������
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); //������ ���������� ���� ��������� ���� � ��� �����
	glutInitWindowSize(512,512); //������ ��������
	glutInitWindowPosition(100,100); //������� ��������
	glutCreateWindow("2048"); //�������� ��������
	Init(); //���� ������� �������������
	glutDisplayFunc(pole); //������� �������, ������� ����������� � ������ ��������� ����
	glutMouseFunc(mouseFunc); //� ������ ������� �������
	glutKeyboardFunc(action); //� ������ ������� ����������
	glutMainLoop(); //����������� ����
	return 0;
};
