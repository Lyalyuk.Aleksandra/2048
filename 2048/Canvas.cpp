#include "Canvas.h"

template<class T>
void swap(T& left, T& right)
{
	T temp = left;
	left = right;
	right = temp;
};

Canvas::Canvas() : value(4, std :: vector<int>(4,0))
{
	srand (time(NULL));
	for (std :: vector< std :: vector<int>> :: iterator p = value.begin(); p != value.end(); ++p)
		for (std :: vector<int> :: iterator c = p->begin(); c != p->end(); ++c)
			*c = 0;

	int i(random(4)),j(random(4));
	value[i][j] = (random(2)&&random(2)) + 1; 
	do
	{
		i = random(4);
		j = random(4);
	}while (value[i][j] != 0);
	value[i][j] = (random(2)&&random(2)) + 1;
	texture = new unsigned[12];
	source[0] = "0.png";
	source[1] = "2.png";
	source[2] = "4.png";
	source[3] = "8.png";
	source[4] = "16.png";
	source[5] = "32.png";
	source[6] = "64.png";
	source[7] = "128.png";
	source[8] = "256.png";
	source[9] = "512.png";
	source[10] = "1024.png";
	source[11] = "2048.png";
	pSt.x = 0;
	pSt.y = 0;
	pSt.flag = false;
}


Canvas::~Canvas()
{
}

int Canvas :: random(int m)
{
	return rand()%m;
}

void Canvas :: load()
{
	unsigned char* data;
	unsigned width, height;

	for (int i=0;i < 12;i++)
	{
		texture[i] = lodepng_decode32_file(&data, &width, &height, source[i]);
		glGenTextures(1,&texture[i]);
		glBindTexture(GL_TEXTURE_2D,texture[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);
	};

	delete data;
}

void Canvas :: draw()
{
	glEnable( GL_TEXTURE_2D );
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (int i = 0;i < 4;i++)
		for(int j  = 0;j < 4;j++)
		{
			glBindTexture( GL_TEXTURE_2D, texture[value[i][j]]);
			glBegin( GL_QUADS );
	
			glTexCoord2f(0,0); glVertex2f(64*i,64*(j+1));
			glTexCoord2f(0,1); glVertex2f(64*i,64*j);
			glTexCoord2f(1,1); glVertex2f(64*(i+1),64*j);
			glTexCoord2f(1,0); glVertex2f(64*(i+1),64*(j+1));

			glEnd();
			glFlush();
		};
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
}

void Canvas :: moveUp()
{
	for (int i = 0,j,p;i < 4;i++)
	{
		j = 3;
		while (j >= 0)
		{
			if (value[j][i] != 0)
			{
				p = j+1;
				while ((p < 4) && (value[p][i]) == 0)
				{
					swap(value[p][i],value[p-1][i]);
					p++;
				}
			}
			j--;
		};	
		j = 3;
		while (j > 0) 
		{
			if ((value[j][i] != 0) && (value[j][i] == value[j-1][i]))
			{
				value[j][i] += 1;
				value[j-1][i] = 0;
				j = j-2;
			} else
				j--;
		};
		j = 3;
		while (j >= 0)
		{
			if (value[j][i] != 0)
			{
				p = j+1;
				while ((p < 4) && (value[p][i]) == 0)
				{
					swap(value[p][i],value[p-1][i]);
					p++;
				}
			}
			j--;
		};	
	};
	randomize();
}

void Canvas :: moveDown()
{
	for (int i = 0,j,p;i < 4;i++)
	{
		j = 0;
		while (j < 4)
		{
			if (value[j][i] != 0)
			{
				p = j-1;
				while ((p >= 0) && (value[p][i]) == 0)
				{
					swap(value[p][i],value[p+1][i]);
					p--;
				}
			}
			j++;
		};	
		j = 0;
		while (j < 3) 
		{
			if ((value[j][i] != 0) && (value[j][i] == value[j+1][i]))
			{
				value[j][i] += 1;
				value[j+1][i] = 0;
				j = j+2;
			} else
				j++;
		};
		j = 0;
		while (j < 4)
		{
			if (value[j][i] != 0)
			{
				p = j-1;
				while ((p >= 0) && (value[p][i]) == 0)
				{
					swap(value[p][i],value[p+1][i]);
					p--;
				}
			}
			j++;
		};	
	};
	randomize();
}

void Canvas :: moveRight()
{
	for (int i = 0,j,p;i < 4;i++)
	{
		j = 3;
		while (j >= 0)
		{
			if (value[i][j] != 0)
			{
				p = j+1;
				while ((p < 4) && (value[i][p]) == 0)
				{
					swap(value[i][p],value[i][p-1]);
					p++;
				}
			}
			j--;
		};	
		j = 3;
		while (j > 0) 
		{
			if ((value[i][j] != 0) && (value[i][j] == value[i][j-1]))
			{
				value[i][j] += 1;
				value[i][j-1] = 0;
				j = j-2;
			} else
				j--;
		};
		j = 3;
		while (j >= 0)
		{
			if (value[i][j] != 0)
			{
				p = j+1;
				while ((p < 4) && (value[i][p]) == 0)
				{
					swap(value[i][p],value[i][p-1]);
					p++;
				}
			}
			j--;
		};	
	};
	randomize();
}

void Canvas :: moveLeft()
{
	for (int i = 0,j,p;i < 4;i++)
	{
		j = 0;
		while (j < 4)
		{
			if (value[i][j] != 0)
			{
				p = j-1;
				while ((p >= 0) && (value[i][p]) == 0)
				{
					swap(value[i][p],value[i][p+1]);
					p--;
				}
			}
			j++;
		};	
		j = 0;
		while (j < 3) 
		{
			if ((value[i][j] != 0) && (value[i][j] == value[i][j+1]))
			{
				value[i][j] += 1;
				value[i][j+1] = 0;
				j = j+2;
			} else
				j++;
		};
		j = 0;
		while (j < 4)
		{
			if (value[i][j] != 0)
			{
				p = j-1;
				while ((p >= 0) && (value[i][p]) == 0)
				{
					swap(value[i][p],value[i][p+1]);
					p--;
				}
			}
			j++;
		};	
	};
	randomize();
}

void Canvas :: randomize()
{
	int i(random(4)),j(random(4));
	while (value[i][j] != 0)
	{
		i = random(4);
		j = random(4);
	}
	value[i][j] = (random(2)&&random(2)) + 1;
}

void Canvas :: move(int x,int y)
{
	if (pSt.flag)
	{
		int b1 = pSt.y - pSt.x, b2 = pSt.y + pSt.x;
		if ((y < x + b1) && (y < -x + b2) && check(left))
			moveLeft();
		else if ((y > x + b1) && (y > -x + b2)&& check(right))
			moveRight();
		else if ((y < x + b1) && (y > -x + b2)&& check(up))
			moveUp();
		else if ((y > x + b1) && (y < -x + b2)&& check(down))
			moveDown();
		pSt.x = x;
		pSt.y = y;
	};
}

void Canvas :: setPrevState(int x,int y)
{
	pSt.flag = true;
	pSt.x = x;
	pSt.y = y;
}

void Canvas :: stopMoving()
{
	pSt.flag = false;
}

bool Canvas :: check(state st)
{
	switch(st)
	{
	case left:
		{
			for (int i(0);i<4;i++)
				for(int j(1);j < 4;j++)
					if (((value[i][j-1] == 0) && (value[i][j] != 0)) || ((value[i][j-1] == value[i][j])&& (value[i][j] != 0)))
						return true;
			return false;
		};
	case up:
		{
			for (int i(0);i<3;i++)
				for(int j(0);j < 4;j++)
					if (((value[i+1][j] == 0) && (value[i][j] != 0)) || ((value[i+1][j] == value[i][j]) && (value[i][j] != 0)))
						return true;
			return false;
		};
	case right:
		{
			for (int i(0);i<4;i++)
				for(int j(0);j < 3;j++)
					if (((value[i][j+1] == 0) && (value[i][j] != 0))|| ((value[i][j+1] == value[i][j])&& (value[i][j] != 0)))
						return true;
			return false;
		};
	case down:
		{
			for (int i(1);i<4;i++)
				for(int j(0);j < 4;j++)
					if (((value[i-1][j] == 0) && (value[i][j] != 0))|| ((value[i-1][j] == value[i][j])&& (value[i][j] != 0)))
						return true;
			return false;
		};
	};
}

bool Canvas :: endChecker()
{
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
		{
			if ((i - 1 >= 0) && (value[i][j] == value[i-1][j]))
				return false;
			if ((i + 1 < 4) && (value[i][j] == value[i+1][j]))
				return false;
			if ((j - 1 >= 0) && (value[i][j] == value[i][j-1]))
				return false;
			if ((j + 1 < 0) && (value[i][j] == value[i][j+1]))
				return false;
			if (value[i][j] == 0)
				return false;
		}
	return true;
}