#pragma once
#include<iostream> //������
#include<stdlib.h>
#include <time.h>
#include<glut.h>
//#include<gl/GLAux.h>
#include<gl/GL.h>
#include<vector>
#include "lodepng.h"

struct PrevState //��������� �����
{
	int x,y;
	bool flag;
};

class Canvas
{
private:
	std :: vector<std :: vector<int>> value; //������ ��������
	unsigned* texture; //������������� ��� ��������
	char* source[12]; //������ ��������
	PrevState pSt; //���������� ��������� �����
	int random(int m); //������
	enum state {left,up,right,down} ; //���������
public:
	Canvas();//����������� �� ���������
	~Canvas(); //����������
	void load();//�������� ��������
	void draw(); //���������
	void moveRight(); //��������� ������
	void moveLeft(); //�������� �����
	void moveUp(); //�����
	void moveDown(); //����
	void move(int x,int y); //���������� ���� ��������� �� ������ ��������� ����
	void randomize(); //����� ������
	void setPrevState(int x,int y); //���������� ���������� ��������� ����
	void stopMoving(); //��������� ���� = false
	bool check(state st); //�������� ����� �� ���������

	bool endChecker();
};

